import React, { Component } from 'react';
import { connect } from 'react-redux';

import ProgressBar from './ProgressBar';

export class Step3 extends Component {

  onClick = () => {
    console.log(this.props.user);
  }

  render() {
    return (
      <div className="form">
        <ProgressBar 
          width={"100%"}
          message="Thank you!"
        />
        <img 
          src="/images/check-mark.png"
          className="check-img"
          alt="check-img"
        />
        <button className="dashboard-btn" onClick={this.onClick}>
          Go to Dashboard
          <img src="/images/right-arrow.png" alt="right-arrow"/>
        </button>
      </div>
    )
  }
}

const mapStateToProps = ({ user }) => ({
  user
});

export default connect(mapStateToProps)(Step3);