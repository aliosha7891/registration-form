import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { fetchSecondInfo } from '../../actions';
import ProgressBar from './ProgressBar';

export class Step2 extends Component {

  state = {
    day: '',
    dayValid: false,
    month: '',
    monthValid: false,
    year: '',
    yearValid: false,
    dateValid: true,
    gender: 'male',
    about: '',
    formValid: false,
    errorMessage: ''
  }

  onDateChange = (e) => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  }

  onGenderChange = (e) => {
    this.setState({
      gender: e.target.value
    });
  }

  onAboutChange = (e) => {
    this.setState({
      about: e.target.value
    });
  }

  validateFields = () => {
    let dayValid = this.state.dayValid;
    let monthValid = this.state.monthValid;
    let yearValid = this.state.yearValid;
    let dateValid = true;
    const { day, month, year } = this.state;

    if ( day >= 1 && day <= 31 ) {
      dayValid = true;
    }

    if ( month >= 1 && month <= 12 ) {
      monthValid = true;
    }

    if ( typeof Number.parseInt(year, 10) === 'number' && 2018 - year >= 18 ) {
      yearValid = true;
    }

    if ( !day || !month || !year ) {
      dateValid = false;
    }

    if ( +month % 2 === 0 && +day > 30 ) {
      dateValid = false;
    }

    if ( +month === 2 && day > 28 ) {
      dateValid = false;
    }

    this.setState({
      dayValid,
      monthValid,
      yearValid,
      dateValid
    }, this.validateForm);

  }

  validateForm = () => {
    const { dayValid, monthValid, yearValid, dateValid } = this.state;
    this.setState({ formValid: dayValid && monthValid && yearValid && dateValid});
  }

  fetchCorrect = () => {
    if (this.state.formValid) {
      const { day, month, year } = this.state;

      this.props.fetchSecondInfo({
        date: Date.parse(`${year}.${month - 1}.${day}`),
        gender: this.state.gender,
        about: this.state.about
      });
      this.props.history.push('/step3');
    } else {
      this.setState({
        errorMessage: 'date is incorrect'
      });
    }
  }

  onSubmit = (e) => {
    e.preventDefault();
    this.validateFields();

    setTimeout(() => this.fetchCorrect(), 0);
  }

  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit} className="form">
          <ProgressBar 
            width={"66%"}
            message="Signup"
          />
          <label className="date-label main-label">
          {
            !this.state.errorMessage ? 
            'date of birth' :
            <label className="danger">{this.state.errorMessage}</label>
          }
          </label>
          <div className="date-form">
            <input 
              type="text"
              autoFocus
              className="date-input"
              value={this.state.day}
              onChange={this.onDateChange}
              placeholder="DD"
              name="day"
              autoComplete="off"
            />
            <input 
              type='text'
              className="date-input"
              value={this.state.month}
              onChange={this.onDateChange}
              placeholder="MM"
              name="month"
              autoComplete="off"
            />
            <input 
              type="text"
              className="date-input"
              value={this.state.year}
              onChange={this.onDateChange}
              placeholder="YYYY"
              name="year"
              autoComplete="off"
            />
          </div>
          <label className="main-label gender-label">gender</label>
          <div className="gender-form">
            <input 
              className="gender-input"
              type="radio"
              value="male"
              checked={this.state.gender === "male"}
              onChange={this.onGenderChange}
              id="male"
            /><label htmlFor="male">male</label>
            <input 
              className="gender-input"
              type="radio"
              value="female"
              checked={this.state.gender === "female"}
              onChange={this.onGenderChange}
              id="female"
            /><label htmlFor="female">female</label>
            <input 
              className="gender-input"
              type="radio"
              value="unspecified"
              checked={this.state.gender === "unspecified"}
              onChange={this.onGenderChange}
              id="unspecified"
            /><label htmlFor="unspecified">unspecified</label>
          </div>
          <label className="main-label about-label">where did you hear about?</label>
          <select className="about-select" onChange={this.onAboutChange}>
            <option></option>
            <option value="from_friends">from friends</option>
            <option value="from_social">from social networks</option>
            <option value="from_university">from university</option>
          </select>
          <hr className="bottom-line"></hr>
          <div className="section-2-btn">
            <Link className="back-btn" to="/">Back</Link>
            <div className="btn">
              <button
                className="next-btn"
                type="submit"
              >Next</button>
              <img src="./images/right-arrow.png" alt="right-arrow" />
            </div>
          </div>
        </form>
      </div>
    )
  }
}

export default connect(undefined, { fetchSecondInfo })(Step2);