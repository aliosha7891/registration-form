import React,{Component} from 'react'
import {Step1} from '../components/RegistrationForm/Step1'
import ProgressBar from '../components/RegistrationForm/ProgressBar'
import {shallow} from 'enzyme'


// create any initia3l state needed
/*const initialState = {}; 
// here it is possible to pass in any middleware if needed into //configureStore
const mockStore = configureStore();
let wrapper;
let store;
beforeEach(() => {
  //creates the store with any initial state or middleware needed  
  store = mockStore(initialState)
  wrapper = shallow(<Step1 store={store}/>)
 })*/
 
 it('Snapshot ',()=>{
   const wrapper = shallow(<Step1/>)
   expect(wrapper).toMatchSnapshot();  
})

it('Sign Up bar displays correctly',()=>{
  const wrapper = shallow(<Step1/>)
  expect(wrapper.contains(<ProgressBar 
    width={"33%"}
    message="Signup"/>)).toEqual(true);  
})
it('States displays correctly',()=>{
  const wrapper = shallow(<Step1/>)
  expect(wrapper.state('formErrors').email).toEqual('')
  expect(wrapper.state('formErrors').password).toEqual('')
  expect(wrapper.state('formErrors').confirm).toEqual('')
})

it('Validation correct ',()=>{
  const wrapper = shallow(<Step1/>)
  wrapper.find('form').simulate('submit',{preventDefault:() =>{}})
  expect(wrapper.state('formErrors').email).toEqual('email is required')
  expect(wrapper.state('formErrors').password).toEqual('password is required')
  expect(wrapper.state('formErrors').confirm).toEqual('confirmation is required')
})


