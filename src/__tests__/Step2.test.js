import React from 'react'
import {Step2} from '../components/RegistrationForm/Step2'
import configureStore from 'redux-mock-store'
import ProgressBar from '../components/RegistrationForm/ProgressBar'
import {shallow} from 'enzyme'
// create any initia3l state needed
/*const initialState = {}; 
// here it is possible to pass in any middleware if needed into //configureStore
const mockStore = configureStore();
let wrapper;
let store;
beforeEach(() => {
  //creates the store with any initial state or middleware needed  
  store = mockStore(initialState)
  wrapper = shallow(<Step1 store={store}/>)
 })*/
 
 it('Snapshot',()=>{
   const wrapper = shallow(<Step2/>)
   expect(wrapper).toMatchSnapshot();  
})

it('Sign Up bar displays correctly',()=>{
 const wrapper = shallow(<Step2/>)
 expect(wrapper.contains(<ProgressBar 
   width={"66%"}
   message="Signup"/>)).toEqual(true);  
})

it('Validation correct',()=>{
  const wrapper = shallow(<Step2/>)
  wrapper.find('form').simulate('submit',{preventDefault:() =>{}})
  expect(wrapper.state('errorMessage')).toEqual('date is incorrect')
})